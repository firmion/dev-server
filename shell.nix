{ pkgs ? import <nixpkgs> {}  }:

with pkgs;

let
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
in

stdenv.mkDerivation {

  name = "dev-server";

  buildInputs = [
    cabal-install
    cabal2nix
    curl
    ghc
    git
    nodejs
    vim
    yarn
    (all-hies.selection { selector = p: { inherit (p) ghc865; };  })
  ];

  shellHook = ''
    export NIX_REMOTE=daemon
    export PATH=$HOME/.yarn/bin:$PATH
  '';

}
