#!/bin/sh

nix-daemon &

USER_NAME=$1

chown -R $USER_NAME:$USER_NAME /package

su -lc  "cd /package && NIX_REMOTE=daemon nix-shell /shell.nix" $USER_NAME

