USER_NAME=$1

adduser -D $USER_NAME

mkdir /nix/var/nix/profiles/per-user/$USER_NAME
ln -s /nix/var/nix/profiles/default /nix/var/nix/profiles/per-user/$USER_NAME/profile
chown -R $USER_NAME:$USER_NAME /nix/var/nix/profiles/per-user/$USER_NAME

mkdir /nix/var/nix/gcroots/per-user/$USER_NAME
chown -R $USER_NAME:$USER_NAME /nix/var/nix/gcroots/per-user/$USER_NAME
