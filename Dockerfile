FROM nixos/nix

ARG USER_NAME=dev-user
ENV USER_NAME=$USER_NAME

COPY scripts/ /scripts

RUN /bin/sh /scripts/create-user.sh $USER_NAME

RUN cp /scripts/vimrc /home/$USER_NAME/.vimrc

RUN (nix-daemon &) && (su -lc "/bin/sh /scripts/update-nix.sh" $USER_NAME)

# For haskell IDE binary cache.
RUN nix-env -iA cachix -f https://cachix.org/api/v1/install
RUN cachix use all-hies

CMD /bin/sh /scripts/start.sh $USER_NAME

